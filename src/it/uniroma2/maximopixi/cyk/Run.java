import java.util.ArrayList;
import java.util.Scanner;


public class Run {

	private static String grammar;
	private static Scanner input;
	private static String word;
	private static Cyk parse;
	private static boolean completeChart;

	/**
	 * Provides simple console interface for CYK parsing
	 * 
	 * @param args
	 */

	public static void main(String[] args) {

		/*
		 * Acquire the grammar
		 */

		System.out.println("\n###################################################################\n" +
				"\nType your grammar 'S-CB_FA_FB;A-CS_FD_a;B-FS_CE_b;C-a_C;D-AA_.;E-BB_E;F-b_.'\n");

		input = new Scanner(System.in);
		grammar = new String();
		grammar = input.next();

		/*
		 * Divide the grammar in productions
		 */

		String[] compactProductions = grammar.split(";");

		/*
		 * Divide the producer by the products
		 */

		ArrayList<String> productions[] = new ArrayList[compactProductions.length];

		String[] leftRight;
		String[] temp;

		for (int x = 0; x < compactProductions.length; x++) {

			productions[x] = new ArrayList<String>();

			leftRight = compactProductions[x].split("-");

			productions[x].add(leftRight[0]);

			temp = (leftRight[1]).split("_");

			for (int y = 0; y < temp.length; y++)
				productions[x].add(temp[y]);	
		}

		// Print each production position to verify input
		for (int i = 0; i < productions.length; i++) {

			System.out.println("\n");

			for (int j = 0; j < productions[i].size(); j++)
				System.out.print(productions[i].get(j) + "\t");

		}
		
		System.out.println("\n");

		/*
		 * Remove epsilon and unit productions
		 */
		
		for(int x = 0; x < productions.length; x++) {
			
			ArrayList<String> managedProduction = Cyk.noEpsilonProduction(productions[x]);

			managedProduction = Cyk.noUnitProductions(managedProduction);
						
			productions[x] = managedProduction;
		}
		
		// Print each production position to verify input
		for (int i = 0; i < productions.length; i++) {

			System.out.println("\n");

			for (int j = 0; j < productions[i].size(); j++)
				System.out.print(productions[i].get(j) + "\t");

		}
		
		
		/*
		 * Acquire the word to test
		 */

		while (true) {

			System.out.println("\n_________________________________________________________\n" +
					"\nType the word you want to test\n");

			word = new String();
			word = input.next();

			System.out.println(word);

			/*
			 * Compute the CYK parsing table
			 */

			int wordDimension = word.length();

			parse = new Cyk(wordDimension);

			// Fill first (banal) row
			for (int j = 0; j < wordDimension; j++) {

				System.out.println("\nComputing cell 0;" + j);

				for (int x = 0; x < productions.length; x++) {

					String insertion = new String();

					for (int y = 1; y < productions[x].size(); y++) {


						String production = productions[x].get(y);
						String wordLetter = Character.toString(word.charAt(j));

						boolean check = production.equals(wordLetter);

						System.out.println("\n" + productions[x].get(y) + " = " + word.charAt(j) + "\t" + check);

						if (check) {

							insertion = productions[x].get(0);

							System.out.print("'" + insertion + "' is now in this cell!\n");

							if((parse.getCell(0, j)) == null)
								parse.setCell(0, j, insertion);
							else
								parse.setCell(0, j, parse.getCell(0, j) + insertion);

						}
					}
				}
			}

			System.out.println("\n_________________________________________________________\n" +
					"\nThis is the associate CYK chart\n");

			completeChart = true;

			// Print first row to verify construction
			for(int q = 0; q < word.length(); q++) {

				System.out.print(parse.getCell(0, q) + "\t\t");

				if(parse.getCell(0, q) == null) {

					completeChart = false;

					break;
				}	
			}

			System.out.println();

			// Construct other elements recursively:

			// for each row
			for(int i = 1; i < wordDimension; i++) {

				if(completeChart == false)
					break;

				// search each CYK original cell,
				for (int j = 0; j < wordDimension-i; j++) {

					if(completeChart == false)
						break;

					// find the associate CYK couples of cells
					for (int k = 0; k < i; k++) {

						if(completeChart == false)
							break;

						String firstCell = parse.getCell(k, j);
						String secondCell = parse.getCell(i-k-1, j+k+1);

						if(firstCell == null || secondCell == null) {

							completeChart = false;
							break;
						}

						// and try concats of each (nonterminal) element from 1st CYK cell of the couple
						for (int n = 0; n < firstCell.length(); n++) {

							char firstNonTerminal = firstCell.charAt(n);

							// with each (nonterminal) element from 2nd CYK cell of the couple
							for (int m = 0; m < secondCell.length(); m++) {

								char secondNonTerminal = secondCell.charAt(m);

								String checkingCouple = Character.toString(firstNonTerminal) + Character.toString(secondNonTerminal); 

								// comparing them with all grammar productions
								for(int x = 0; x < compactProductions.length; x++) {

									for (int y = 1; y < productions[x].size(); y++) {

										String productionToCheck = productions[x].get(y);

										// and saving the producer in the CYK original cell
										if(checkingCouple.equals(productionToCheck)) {

											if(parse.getCell(i, j) == null)
												parse.setCell(i, j, productions[x].get(0));
											else {

												boolean thereIs = false;

												for(int d = 0; d < parse.getCell(i, j).length(); d++) {

													if(parse.getCell(i, j).charAt(d) == productions[x].get(0).toCharArray()[0])
														thereIs = true;

													break;
												}

												if(thereIs == false)
													parse.setCell(i, j, parse.getCell(i, j) + productions[x].get(0));
											}	
											break;
										}
									}
								}
							}								
						}
					}

					System.out.print(parse.getCell(i, j) + "\t\t");
				}

				System.out.println();
			}

			/*
			 * Check if the word belongs to the language
			 */
			String verificationCell = parse.getCell(word.length()-1, 0);
			
			if(completeChart == false || verificationCell == null)
				System.out.println("\nUncompletable parsing chart: this word doesn't belong to the language! KO\n");
			else {

				char startingSymbol = productions[0].get(0).toCharArray()[0];

				boolean belongness = false;

				for(int p = 0; p < verificationCell.length(); p++) {

					if(verificationCell.charAt(p) == startingSymbol){

						belongness = true;
						break;
					}
				}

				if(belongness == true)
					System.out.println("\nThe word belongs to the language! OK\n");
				else
					System.out.println("\nThe word doesn't belong to the language! KO\n");
			}
		}
	}
}

