import java.util.ArrayList;
import java.util.Arrays;


/**
 * Handles strings approaching them like productions, and manages them for a CYK parsing
 * 
 * Legend:
 * '-' 	to separate producer by products;
 * '_' 	to separate products by other products;
 * ';'	to separate different productions;
 * '.'	for epsilon productions.
 * 
 * @author Pizzi Massimo
 */
public class Cyk {

	private String[][] parsingTable;

	private static ArrayList<String> production;

	private static String epsilon = ".";



	Cyk (int dimension) {

		parsingTable = new String [dimension][dimension];
	}


	public String getCell(int row, int column) {

		return parsingTable[row][column];
	}


	public void setCell(int row, int column, String nonTerminal) {

		this.parsingTable[row][column] = nonTerminal;
	}


	/**
	 * Removes unit-productions
	 * 
	 * @param p		A generic production
	 * 
	 * @return The production without unit-production
	 */
	public static  ArrayList<String> noUnitProductions (ArrayList<String> p) {

		production = p;


		System.out.println("Unit-p check");
		for(int s = 1; s < production.size(); s++) {

			System.out.println(s);
			if(production.get(s).equals(p.get(0))) {


				production.remove(s);

				s--;
			}
		}

		return production;
	}


	/**
	 * Removes epsilon-productions
	 * 
	 * @param p		A generic production
	 * 
	 * @return The unextended production
	 */
	public static  ArrayList<String> noEpsilonProduction (ArrayList<String> p) {

		production = p;


		System.out.println("Epsilon-p check");
		for(int s = 1; s < production.size(); s++) {

			System.out.println(epsilon + production.get(s) + production.get(s).equals(epsilon));
			if(production.get(s).equals(epsilon)) {

				production.remove(s);

				s--;
			}
		}
		return p;
	}


	/**
	 * Computes the Chomsky normal-form if possible
	 * 
	 * @return The equivalent grammar in Chomsky normal-form
	 */
	public ArrayList<ArrayList<String>> ChomskyNf (ArrayList<String> p[]) {

		char newNt = 'A';
		boolean used = false;


		/*
		 * Populate the arraylist of arraylist representing the grammar
		 */
		ArrayList<ArrayList <String>> cNFProd = new ArrayList();

		for(int l = 0; l < p.length; l++) {

			cNFProd.add(new ArrayList());

			for(int r = 0; r < p[l].size(); r++)

				cNFProd.get(l).add(p[l].get(r));	

		}

		/*
		 * Compute the CNF checking each production
		 */
		for(int l = 0; l < cNFProd.size(); l++) {

			for(int r = 0; r < cNFProd.get(l).size(); r++) {

				String checkingProd = cNFProd.get(l).get(r);

				if(checkingProd.length() > 2) {

					//	find a new possible non terminal
					for(int e = 0; e < cNFProd.size(); e++) {

						if(cNFProd.get(e).get(0).equals(Character.toString(newNt))) {

							newNt++;
							e = 0;
						}
					}

					//	add the new production
					int newProdIndex = cNFProd.size();
					cNFProd.add(new ArrayList());
					cNFProd.get(newProdIndex).add(Character.toString(newNt));
					cNFProd.get(newProdIndex).add(checkingProd.substring(1, checkingProd.length()));

					//	modify previous production
					cNFProd.get(l).set(r, checkingProd.substring(0, 1) + Character.toString(newNt));
				}
			}
		}

		/*
		 * Promote terminal productions
		 */
		for(int l = 0; l < cNFProd.size(); l++) {

			for(int r = 0; r < cNFProd.get(l).size(); r++) {

				String checkingProd = cNFProd.get(l).get(r);

				for(int w = 0; w < checkingProd.length(); w++) {

					if(Character.isLowerCase(checkingProd.charAt(w))) {

						//	find a new possible non terminal
						for(int e = 0; e < cNFProd.size(); e++) {

							if(cNFProd.get(e).get(0).equals(Character.toString(newNt))) {

								newNt++;
								e = 0;
							}
						}

						//	add the new production
						int newProdIndex = cNFProd.size();
						cNFProd.add(new ArrayList());
						cNFProd.get(newProdIndex).add(Character.toString(newNt));
						cNFProd.get(newProdIndex).add(Character.toString(checkingProd.charAt(w)));

						//	modify previous production
						cNFProd.get(l).set(r, checkingProd.replace(checkingProd.charAt(w), newNt));
					}
				}
			}
		}

		return cNFProd;
	}

}